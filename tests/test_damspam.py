# SPDX-Licences-Identifier: MIT

from damspam import (
    not_read_only,
    Builder,
    GitlabUrl,
    Issue,
    Note,
    Project,
    User,
    UserList,
    BuilderError,
    ProtectedUserList,
    DamspamRequestWebhookTemplate,
    SafetyFlags,
    Secrets,
    SecretsUpdateError,
    FDO_BOTS_PROJECT_ID,
    SPAMBOT,
    BUGBOT,
    BOT_MESSAGE,
)
from typing import Optional
from unittest.mock import patch, MagicMock
from datetime import datetime, timedelta, timezone
from pathlib import Path

import attr
import logging
import pytest
import queue
import subprocess
import yaml

logger = logging.getLogger("test-cli")


@pytest.mark.parametrize("readonly", (True, False))
def test_if_not_readonly(caplog, readonly):
    caplog.set_level(logging.INFO, "damspam")

    ro = not_read_only(readonly, "Some message")
    assert isinstance(ro, bool)
    assert ro != readonly
    assert "Some message" in caplog.text
    if readonly:
        assert "[RO SKIP]" in caplog.text
    else:
        assert "[RO SKIP]" not in caplog.text


@pytest.mark.parametrize("readonly", (True, False))
@patch("gitlab.Gitlab")
def test_builder(gitlab, readonly):
    @attr.s
    class Status:
        auth_called: bool = attr.ib(init=False, default=False)
        project: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore
        issue: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore
        issue_note: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore
        tracker_issue: MagicMock = attr.ib(init=False, default=attr.Factory(MagicMock))  # type: ignore

    status = Status()

    def auth_called():
        status.auth_called = True

    gitlab.auth = auth_called
    builder = Builder.create_from_instance(gitlab)
    assert not status.auth_called, "Creating from instance must not call auth()"

    def projects_get(**kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == "test/foo"
        return status.project

    gitlab.projects = MagicMock()
    gitlab.projects.get = projects_get
    builder.set_project("test/foo")

    # Our API doesn't care if you call set_project twice, let's make use of that here

    def projects_get_id(**kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 457
        return status.project

    gitlab.projects = MagicMock()
    gitlab.projects.get = projects_get_id
    builder.set_project_id(457)
    assert builder.project == status.project

    def issues_get(*args, **kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 123
        return status.issue

    status.project.issues = MagicMock()
    status.project.issues.get = issues_get

    def issue_notes_get(*args, **kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 987
        return status.issue_note

    status.issue.notes = MagicMock()
    status.issue.notes.get = issue_notes_get

    builder.set_issue_iid(123)
    assert builder.issue == status.issue

    builder.set_note_id(987)
    assert builder.note == status.issue_note

    builder.set_readonly(readonly)
    assert builder.readonly == readonly

    def projects_get_tracker(**kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == "tracker/project"
        return status.project

    def issues_get_tracker(*args, **kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 99
        return status.tracker_issue

    gitlab.projects.get = projects_get_tracker
    status.project.issues.get = issues_get_tracker
    builder.set_tracker_project_issue("tracker/project", 99)
    assert builder.tracker_issue == status.tracker_issue

    # tracker == issue check
    with pytest.raises(BuilderError):
        builder.set_tracker_issue(status.issue)

    issue = builder.build_issue()
    assert issue.gl == gitlab
    assert issue.project == status.project
    assert issue.issue == status.issue
    assert issue.tracker_issue == status.tracker_issue
    assert issue.readonly == readonly

    issue_note = builder.build_note()
    assert issue_note.gl == gitlab
    assert issue_note.project == status.project
    assert issue_note.parent == status.issue
    assert issue_note.note == status.issue_note
    assert issue_note.readonly == readonly


@pytest.mark.parametrize("readonly", (True, False))
def test_link_to_tracker(readonly):
    gl, project, issue, tracker_issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    project.name = "pytest run"
    tracker_issue.title = "tracker issue title"
    tracker_issue.project_id = 1
    tracker_issue.iid = 99

    links = []

    issue.links = MagicMock()
    issue.links.create = lambda args: links.append(args)

    damspam = Issue(gl, project, issue, tracker_issue, readonly)
    damspam.link_to_tracker()
    if readonly:
        # expect nothing to happen
        assert links == []
    else:
        # epxect the links to be called with the tracker issue data
        assert links == [
            {
                "target_project_id": tracker_issue.project_id,
                "target_issue_iid": tracker_issue.iid,
            }
        ]

    # expect nothing to happen if we don't have a tracker issue,
    # regardless of readonly setting
    links = []
    damspam.tracker_issue = None
    damspam.link_to_tracker()
    assert links == []

    damspam.tracker_issue = issue
    with pytest.raises(AssertionError):
        damspam.link_to_tracker()


@pytest.mark.parametrize("readonly", (True, False))
def test_mark_as_spam(readonly):
    gl, project, issue, tracker_issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    issue.assignee_ids = [1, 2, 3]
    issue.state = "open"
    project.name = "pytest run"
    tracker_issue.title = "tracker issue title"
    tracker_issue.project_id = 1
    tracker_issue.iid = 99

    @attr.s
    class Status:
        saved: bool = attr.ib(default=False)

    status = Status()

    def issue_save(*args, **kwargs):
        assert not args
        assert not kwargs
        status.saved = True
        if issue.state_event == "close":
            issue.state = "closed"
            issue.state_event = None
        elif issue.state_event == "reopen":
            issue.state = "open"
            issue.state_event = None

    issue.labels = ["existing-label"]
    issue.confidential = False
    issue.save = issue_save

    damspam = Issue(gl, project, issue, tracker_issue, readonly=readonly)
    damspam.mark_as_spam()
    if readonly:
        # expect nothing to happen
        assert issue.labels == ["existing-label"]
        assert issue.confidential is False
        assert issue.assignee_ids == [1, 2, 3]
        assert status.saved is False
        assert issue.state == "open"
    else:
        assert sorted(issue.labels) == sorted(["existing-label", "Spam"])
        assert issue.confidential is True
        assert issue.assignee_ids == [SPAMBOT.id]
        assert status.saved is True
        assert issue.state == "closed"

    damspam.tracker_issue = issue
    with pytest.raises(AssertionError):
        damspam.mark_as_spam()


@pytest.mark.parametrize("readonly", (True, False))
@pytest.mark.parametrize(
    "test_mode", ("spammer", "old-account", "already-blocked", "conversation")
)
@pytest.mark.parametrize("ignore_safety_checks", (True, False))
def test_block_issue_creator(
    ignore_safety_checks: bool, test_mode: str, readonly: bool
):
    gl, project, issue, tracker_issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    @attr.s
    class Status:
        blocked: bool = attr.ib(default=False)
        notes: Optional[str] = attr.ib(default=None)

    status = Status()

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    issue.author = {
        "id": 987,
        "state": "active",
        "name": "mr naughty",
    }
    if test_mode == "already-blocked":
        issue.author["state"] = "blocked"
    else:
        issue.author["state"] = "active"
    issue.notes = MagicMock()
    project.name = "pytest run"
    tracker_issue.title = "tracker issue title"
    tracker_issue.project_id = 1
    tracker_issue.iid = 99

    def notes_create(*args):
        assert not readonly, "Must not be called in readonly"
        # Dunno why this takes a single dict arg but oh well
        assert len(args) == 1
        assert isinstance(args[0], dict)
        assert args[0]["body"] == BOT_MESSAGE
        assert status.notes is None
        status.notes = args[0]["body"]

    issue.notes.create = notes_create
    if test_mode == "conversation":
        c1 = MagicMock()
        c1.author = {"id": 1}
        c2 = MagicMock()
        c2.author = {"id": 2}
        c3 = MagicMock()
        c3.author = {"id": 3}
        issue.notes.list = lambda: [c1, c2, c3]
    else:
        issue.notes.list = lambda: []

    def user_block(*args):
        assert not args
        assert not readonly, "Must not be called in readonly"
        status.blocked = True

    spammer = MagicMock()
    spammer.block = user_block
    if test_mode == "old-account":
        dt = timedelta(weeks=26)
        spammer.created_at = datetime.isoformat(datetime.now(tz=timezone.utc) - dt)
    else:
        spammer.created_at = datetime.isoformat(datetime.now(tz=timezone.utc))

    def users_get(*args):
        assert args[0] == 987
        return spammer

    gl.users = MagicMock()
    gl.users.get = users_get

    # Now set us up to ignore whatever we test for
    if ignore_safety_checks:
        ignore_flags = SafetyFlags(
            {
                "spammer": 0,
                "old-account": SafetyFlags.ACCOUNT_AGE,
                "already-blocked": SafetyFlags.ALREADY_BLOCKED,
                "conversation": SafetyFlags.CONVERSATION_LENGTH,
            }[test_mode]
        )
        safety_flags = SafetyFlags.ignore(ignore_flags)
    else:
        safety_flags = SafetyFlags.all()

    print("ignore:", ignore_safety_checks)
    print(test_mode)
    print(safety_flags)

    damspam = Issue(gl, project, issue, tracker_issue, readonly)
    damspam.block_issue_creator(safety_flags=safety_flags)
    if readonly:
        assert status.blocked is False
    else:
        if test_mode == "spammer" or ignore_safety_checks:
            assert status.blocked is True
            assert status.notes is None  # no notes on success
        else:
            assert status.blocked is False
            assert status.notes == BOT_MESSAGE


@pytest.mark.parametrize("readonly", (True, False))
def test_hide_note_as_spam(readonly):
    gl, project, issue, issue_note = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )
    spam_content = "buy this < or > that"
    escaped = "buy this &lt; or &gt; that"
    hidden_spam = f"*spam removed*\n\n<!-- {escaped[::-1]}\n-->"

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    issue.assignee_ids = [1, 2, 3]
    issue.state = "open"
    project.name = "pytest run"
    issue_note.id = 987
    issue_note.body = spam_content

    @attr.s
    class Status:
        saved: bool = attr.ib(default=False)

    status = Status()

    def issue_note_save(*args, **kwargs):
        assert not args
        assert not kwargs
        status.saved = True

    issue_note.save = issue_note_save

    damspam = Note(gl, project, issue, issue_note, readonly=readonly)
    damspam.hide_as_spam()
    if readonly:
        # expect nothing to happen
        assert issue_note.body == spam_content
        assert status.saved is False
    else:
        assert issue_note.body == hidden_spam
        assert status.saved is True


@pytest.mark.parametrize("emoji", ("do_not_litter", ":do_not_litter:"))
@pytest.mark.parametrize("other", ([], [":poop:"]))
def test_note_has_emoji(emoji, other):
    gl, project, issue, issue_note = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    # API returns names without :
    escaped = [emoji.replace(":", "")] if emoji else []

    @attr.s
    class Awardable:
        name: str = attr.ib()

    all_emojis = [Awardable(e) for e in (other + escaped)]
    issue_note.awardemojis.list.return_value = all_emojis
    note = Note(gl, project, issue, issue_note, False)
    if emoji:
        assert note.has_emoji(emoji)


@pytest.mark.parametrize("readonly", (True, False))
def test_delete_user(readonly):
    gl, user, issues = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    @attr.s
    class Status:
        deleted: bool = attr.ib(default=False)

    status = Status()

    def user_delete(*args, **kwargs):
        if readonly:
            assert False, "Delete must not be called in readonly"
        else:
            assert "hard_delete" in kwargs
            assert kwargs["hard_delete"] is True
            status.deleted = True

    user.id = 123
    user.is_admin = False
    user.delete = user_delete
    dsuser = User(gl, user, issues, readonly=readonly)
    dsuser.delete()
    if readonly:
        assert status.deleted is False
    else:
        assert status.deleted is True

    status.deleted = False
    user.id = SPAMBOT.id
    dsuser = User(gl, user, issues, readonly=readonly)
    dsuser.delete()
    assert status.deleted is False

    user.id = 123
    user.is_admin = True
    dsuser = User(gl, user, issues, readonly=readonly)
    dsuser.delete()
    assert status.deleted is False


@pytest.mark.parametrize("readonly", (True, False))
@pytest.mark.parametrize("test_mode", ("spammer", "old-account"))
@pytest.mark.parametrize("ignore_safety_checks", (True, False))
@pytest.mark.parametrize("account_state", ("active", "blocked"))
def test_block_issue_note_creator(
    ignore_safety_checks: bool, test_mode: str, readonly: bool, account_state: str
):
    gl, project, issue, issue_note = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    @attr.s
    class Status:
        blocked: bool = attr.ib(default=False)
        notes: Optional[str] = attr.ib(default=None)

    status = Status()

    project.name = "pytest run"

    issue.id = 123
    issue.iid = 567
    issue.title = "some test issue"
    issue.author = {
        "id": 404,
        "name": "issue creator",
    }

    issue_note.id = 987
    issue_note.author = {
        "id": 1001,
        "state": account_state,
        "name": "mr naughty",
    }

    def issue_notes_get(*args, **kwargs):
        assert "id" in kwargs
        assert kwargs["id"] == 987
        return issue_note

    issue.notes.get = issue_notes_get

    def user_block(*args):
        assert not args
        assert not readonly, "Must not be called in readonly"
        status.blocked = True

    spammer = MagicMock()
    spammer.block = user_block
    if test_mode == "old-account":
        dt = timedelta(weeks=26)
        spammer.created_at = datetime.isoformat(datetime.now(tz=timezone.utc) - dt)
    else:
        spammer.created_at = datetime.isoformat(datetime.now(tz=timezone.utc))

    def users_get(*args):
        assert args[0] == 1001
        return spammer

    gl.users = MagicMock()
    gl.users.get = users_get

    # Now set us up to ignore whatever we test for
    if ignore_safety_checks:
        ignore_flags = SafetyFlags(
            {
                "spammer": 0,
                "old-account": SafetyFlags.ACCOUNT_AGE,
            }[test_mode]
        )
        safety_flags = SafetyFlags.ignore(ignore_flags)
    else:
        safety_flags = SafetyFlags.all()

    damspam = Note(gl, project, issue, issue_note, readonly)
    retval = damspam.block_creator(safety_flags=safety_flags)
    if readonly:
        assert status.blocked is False
    else:
        if test_mode == "spammer" or ignore_safety_checks:
            # for already blocked users we never call into block()
            assert status.blocked == (account_state != "blocked")
        else:
            assert status.blocked is False
            # blocking blocked users ialways returns True
            assert retval == (account_state == "blocked")


@pytest.mark.parametrize("readonly", (True, False))
@pytest.mark.parametrize("external", (True, False))
@pytest.mark.parametrize("only_external", (True, False))
def test_sanitize_user(readonly, external, only_external):
    gl, user, issues = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    logger.debug(f"readonly {readonly}, external {external}, only {only_external}")

    reset_fields = [
        "discord",
        "bio",
        "twitter",
        "linkedin",
        "website_url",
        "location",
    ]

    @attr.s
    class Status:
        sanitized: bool = attr.ib(default=False)

    status = Status()

    def user_save(*args, **kwargs):
        if readonly:
            assert False, "Save must not be called in readonly"
        elif user.external or not only_external:
            for field in reset_fields:
                assert getattr(user, field) == "", f"for field {field}"
            assert "Profile sanitized by damspam" in user.note
            status.sanitized = True

    user.id = 123
    user.is_admin = False
    user.external = external
    user.save = user_save

    for field in reset_fields:
        setattr(user, field, "random data")

    dsuser = User(gl, user, issues=[], readonly=readonly)
    dsuser.sanitize(only_external=only_external)
    if readonly:
        assert status.sanitized is False
    elif only_external and not external:
        assert status.sanitized is False
    else:
        assert status.sanitized is True

    status.sanitized = False
    user.id = SPAMBOT.id
    dsuser = User(gl, user, issues, readonly=readonly)
    dsuser.sanitize()
    assert status.sanitized is False

    user.id = 123
    user.is_admin = True
    dsuser = User(gl, user, issues, readonly=readonly)
    dsuser.sanitize()
    assert status.sanitized is False


@pytest.mark.parametrize("readonly", (True, False))
def test_list_spammers(readonly):
    gl = MagicMock()

    spammer = MagicMock()
    spammer.id = 567
    spammer.name = "naughty lad"
    spammer.username = "xdfsa"

    def users_list(*args, **kwargs):
        assert kwargs["blocked"] is True
        return [spammer]

    gl.users = MagicMock()
    gl.users.list = users_list

    issue = MagicMock()
    issue.id = 99
    issue.iid = 9
    issue.title = "LOOK AT ME"

    def issues_list(*args, **kwargs):
        assert kwargs["author_id"] == 567
        assert kwargs["scope"] == "all"
        assert kwargs["labels"] == "Spam"
        return [issue]

    gl.issues.list = issues_list

    dam = UserList(gl, readonly)
    spammers = dam.list_spammers()
    assert len(spammers) == 1, "Expected only one user in the spammer list"
    spammer = spammers[0]
    assert spammer.readonly == readonly
    assert spammer.user.id == 567

    assert len(spammer.issues) == 1, "Expected only one spam issue"
    spam = spammer.issues[0]
    assert spam.id == 99


@pytest.mark.parametrize("readonly", (True, False))
def test_list_profile_spammers(readonly):
    gl = MagicMock()

    spammer = MagicMock()
    spammer.id = 567
    spammer.name = "naughty lad"
    spammer.username = "xdfsa"
    spammer.bio = "blah"
    spammer.website_url = "https://spam.site"
    spammer.external = True

    user = MagicMock()
    user.id = 123
    user.name = "brave lad"
    user.username = "abcd"
    user.bio = None
    user.website_url = None
    user.external = True

    internal = MagicMock()
    internal.id = 999
    internal.name = "internal"
    internal.username = "internal_user1"
    internal.bio = "blah"
    internal.website_url = "https://legit.site"
    internal.external = False

    users = [spammer, user, internal]

    def users_list(*args, **kwargs):
        return users

    gl.users = MagicMock()
    gl.users.list = users_list
    gl.users.get = lambda id: [u for u in users if u.id == id].pop(0)

    q = queue.Queue()
    dam = UserList(gl, readonly)
    dam.list_profile_spammers(queue=q)

    assert not q.empty()
    spammer = q.get()
    q.task_done()
    assert spammer.readonly == readonly
    assert spammer.user.id == 567

    # We expect a terminating None
    assert not q.empty()
    noneval = q.get()
    q.task_done()
    assert noneval is None

    assert q.empty()


@pytest.mark.parametrize("username", ("ghost", "spambot"))
def test_protected_users(username):
    userlist = ProtectedUserList.instance()
    if username == "ghost":
        protected = userlist.ghost
    elif username == "spambot":
        protected = userlist.spambot
    else:
        assert False, "Unexpected argument"
    assert protected is not None
    assert protected.name == username
    assert userlist.lookup_user_by_name(username) is not None

    # other names are not protected
    not_protected = userlist.lookup_user_by_name("foobar")
    assert not_protected is None
    not_protected = userlist.lookup_user_by_id(123)
    assert not_protected is None


@pytest.mark.parametrize("access_level", (0, 5, 10, 20, 30, 40, 50, 60))
@pytest.mark.parametrize("membership", ("nonmember", "member", "admin"))
@pytest.mark.parametrize("readonly", (True, False))
def test_damspam_project(readonly, membership, access_level):
    import gitlab

    gl, project, user = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    admin = MagicMock()
    admin.is_admin = membership == "admin"
    gl.users.get.return_value = admin

    def members_all_get(id: int) -> MagicMock:
        if membership != "member":
            raise gitlab.exceptions.GitlabGetError()

        member_user = MagicMock()
        member_user.access_level = gitlab.const.AccessLevel(access_level)
        member_user.id = 1234
        member_user.username = "testuser"
        return user

    project.members_all = MagicMock()
    project.members_all.get = members_all_get
    project.path_with_namespace = "test/project"
    project.id = 99

    user = MagicMock()
    user.access_level = gitlab.const.AccessLevel(access_level)
    user.id = 1234
    user.username = "testuser"

    proj = Project(gl=gl, project=project, user=user, readonly=readonly)
    is_maintainer = proj.user_is_maintainer()
    if membership == "nonmember":
        assert is_maintainer is False
    elif membership == "member":
        assert is_maintainer == (access_level >= 40)
    elif membership == "admin":
        assert is_maintainer is True

    @attr.s
    class FakeReply:
        web_url: str = attr.ib()

    fdo_bots = MagicMock()
    fdo_bots.issues.create.return_value = FakeReply("yep")

    projects = MagicMock()
    projects.get.return_value = fdo_bots
    gl.projects = projects

    template = DamspamRequestWebhookTemplate("test/project", 99, 1234, "testuser")

    proj.file_webhook_request_issue(template)
    projects.get.assert_called_once_with(id=FDO_BOTS_PROJECT_ID)
    if readonly:
        fdo_bots.issues.create.assert_not_called()
    else:
        fdo_bots.issues.create.assert_called()
        args = fdo_bots.issues.create.call_args.args
        assert args[0]["title"] == template.title
        assert args[0]["description"] == template.body


@pytest.mark.parametrize("bot", (BUGBOT, SPAMBOT))
def test_request_template_title_parsing(bot):
    title = f"{bot.project} webhook request: foo/bar"
    project = bot.template_class.project_from_title(title)
    assert project == "foo/bar"

    title = f"{bot.project} webhook request: foo/bar/bat"
    project = bot.template_class.project_from_title(title)
    assert project == "foo/bar/bat"

    title = f"{bot.project} webhook request: bar"
    project = bot.template_class.project_from_title(title)
    assert project is None

    title = f"{bot.project} webhook request: foo/bar.with.dots"
    project = bot.template_class.project_from_title(title)
    assert project == "foo/bar.with.dots"

    for title in (
        # missing space
        f"{bot.project} webhook request:foo/bar",
        # wrong prefix
        f"{bot.project} webhook: foo/bar",
        # garbage at end of title
        f"{bot.project} webhook request: foo/bar bas bat"
        # no prefix
        "foo/bar",
    ):
        project = bot.template_class.project_from_title(title)
        assert project is None

    # make sure we only parse the one for our bot
    for prefix in ["damspam", "bugbot"]:
        title = f"{prefix} webhook request: foo/bar"
        project = bot.template_class.project_from_title(title)

        if bot.project == prefix:
            assert project == "foo/bar"
        else:
            assert project is None


@pytest.mark.parametrize("readonly", (True, False))
def test_add_webhook(readonly):
    gl, project = (
        MagicMock(),
        MagicMock(),
    )

    project.hooks.create.return_value = "200"

    ds_project = Project(gl=gl, project=project, user=None, readonly=readonly)
    ds_project.add_project_webhook(url="some test url", token="top secret")
    if readonly:
        project.hooks.create.assert_not_called()
    else:
        project.hooks.create.assert_called_with(
            {
                "url": "some test url",
                "token": "top secret",
                "push_events": False,
                "issues_events": True,
                "confidential_issues_events": True,
                "merge_requests_events": True,
                "emoji_events": True,
                "enable_ssl_verification": False,
            }
        )


@pytest.mark.parametrize("readonly", (True, False))
@pytest.mark.parametrize(
    "path_with_namespace",
    (
        "foo/bar",
        "baz/bar/bat",
        "baz/bar/bat.with.dots",
        "https://example.com/foo/bar",
        "https://example.com/foo/bar.with.dots",
    ),
)
@pytest.mark.parametrize("bot", (SPAMBOT, BUGBOT))
def test_update_webhook(readonly, path_with_namespace, bot):
    gl, project = (
        MagicMock(),
        MagicMock(),
    )

    project.path_with_namespace = path_with_namespace
    token = "magic token, do not touch"
    url = bot.project_webhook(path_with_namespace)

    hook = MagicMock()
    hook.url = url
    hook.token = token

    project.hooks.list.return_value = [hook]

    ds_project = Project(gl=gl, project=project, user=None, readonly=readonly)
    ds_project.update_bot_webhook(bot)

    project.hooks.create.assert_not_called()
    if readonly:
        hook.save.assert_not_called()
    else:
        hook.save.assert_called_with()
        assert hook.push_events is False
        assert hook.issues_events is True
        assert hook.merge_requests_events is True
        assert hook.confidential_issues_events is True
        assert hook.emoji_events is True
        assert hook.enable_ssl_verification is False

        assert hook.url == url
        assert hook.token == token


@pytest.mark.parametrize("readonly", (True, False))
@pytest.mark.parametrize("close", (True, False))
@pytest.mark.parametrize("success", (None, True, False))
@pytest.mark.parametrize("label_prefix", ("damspam", "foo"))
def test_update_issue(readonly, close, success, label_prefix):
    gl, project, issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )
    issue.labels = []
    issue.assignee_ids = [9999]

    ds_issue = Issue(
        gl=gl, project=project, issue=issue, tracker_issue=None, readonly=readonly
    )
    ds_issue.update_with_message(
        message="foo",
        close=close,
        is_success=success,
        label_prefix=label_prefix,
        assign_to=1234,
    )
    if readonly:
        issue.notes.create.assert_not_called()
        issue.notes.save.assert_not_called()
        issue.assignee_ids = [9999]
    else:
        issue.notes.create.assert_called_with({"body": "foo"})
        assert issue.assignee_ids == [1234]
        if close:
            assert issue.state_event == "close"
        assert label_prefix in issue.labels
        if success is True:
            assert f"{label_prefix}::success" in issue.labels
        else:
            assert f"{label_prefix}::success" not in issue.labels

        if success is False:
            assert f"{label_prefix}::failed" in issue.labels
        else:
            assert f"{label_prefix}::failed" not in issue.labels

        if success is None:
            assert f"{label_prefix}::success" not in issue.labels
            assert f"{label_prefix}::failed" not in issue.labels
        issue.save.assert_called()


@pytest.mark.parametrize(
    "fail_case",
    (
        "is-maintainer",
        "is-admin",
        "title-wrong",
        "not-maintainer",
        "already-assigned",
        "hook-exists",
        "git-error",
    ),
)
@pytest.mark.parametrize("bot", (BUGBOT, SPAMBOT))
@pytest.mark.parametrize("readonly", (True, False))
@pytest.mark.parametrize(
    "path_with_namespace", ["foo/bar", "foo/bar/baz", "baz/bar.with.dots"]
)
@patch("damspam.Secrets")
def test_process_webhook_request(
    secrets, path_with_namespace, readonly, bot, fail_case
):
    from gitlab.const import AccessLevel

    gl, project, issue = (
        MagicMock(),
        MagicMock(),
        MagicMock(),
    )

    admin = MagicMock()
    gl.users.get.return_value = admin
    admin.is_admin = fail_case == "is-admin"

    # secrets is damspam.Secrets, after we call clone on it we get the result
    # that we call the methods on so we need to mock this separately
    cloned_secrets = MagicMock()
    cloned_secrets.__enter__.return_value = cloned_secrets
    cloned_secrets.__exit__.return_value = False
    secrets.clone.return_value = cloned_secrets

    issue.state_event = None

    maintainer_user = MagicMock()
    maintainer_user.id = 1234
    maintainer_user.access_level = (
        AccessLevel.MAINTAINER
        if fail_case != "not-maintainer"
        else AccessLevel.DEVELOPER
    )

    foobar_project = MagicMock()
    foobar_project.members_all.get.return_value = maintainer_user
    foobar_project.name = path_with_namespace

    def projects_get(id):
        assert id == path_with_namespace
        return foobar_project

    gl.projects.get = projects_get

    if fail_case == "git-error":
        cloned_secrets.push = MagicMock(side_effect=SecretsUpdateError("oops"))

    if fail_case == "title-wrong":
        issue.title = f"request: {path_with_namespace}"
    else:
        issue.title = f"{bot.project} webhook request: {path_with_namespace}"

    if fail_case == "already-assigned":
        issue.assignees = [{"id": 456}, {"id": bot.id}]
    else:
        issue.assignees = [{"id": 456}, {"id": 789}]

    ds_issue = Issue(
        gl=gl, project=project, issue=issue, tracker_issue=None, readonly=readonly
    )

    if fail_case == "hook-exists":
        hook = MagicMock()
        hook.url = bot.project_webhook(foobar_project.name)
        foobar_project.hooks.list.return_value = [hook]

    result = ds_issue.process_webhook_request(bot)
    if fail_case in ["is-maintainer", "is-admin"]:
        assert result is None
    else:
        assert result is not None

    if readonly or fail_case not in ["is-maintainer", "is-admin"]:
        foobar_project.hooks.create.assert_not_called()
        issue.notes.create.assert_not_called()
        assert issue.state_event is None
        issue.save.assert_not_called()
    else:
        if fail_case in ["hook-exists"]:
            secrets.clone.assert_not_called()
            cloned_secrets.add_project_entry.assert_not_called()
            cloned_secrets.push.assert_not_called()
            foobar_project.hooks.create.assert_not_called()
        else:
            secrets.clone.assert_called_once()
            kwargs = secrets.clone.call_args.kwargs
            assert kwargs["readonly"] == readonly

            cloned_secrets.add_project_entry.assert_called_once()
            args = cloned_secrets.add_project_entry.call_args.args
            assert args[0] == path_with_namespace
            # Token is invisible to us, can't check

            cloned_secrets.push.assert_called_once()

            foobar_project.hooks.create.assert_called()

        # Closing the issue is done by the CLI
        issue.notes.create.assert_not_called()
        assert issue.state_event is None
        issue.save.assert_not_called()


def init_fake_git_repo(path: Path, repo_name: Path, submodule_name: Path) -> Path:
    # Fake a git repo that mirrors the upstream fdo-bots repo layout. That repository
    # has a helm-gitlab-secrets submodule that has a damspam.yaml file at its root, i.e.
    # fdo-bots/
    #    helm-gitlab-secrets/damspam.yaml
    repo_dir = path / repo_name
    repo_dir.mkdir()
    submodule_dir = path / submodule_name
    submodule_dir.mkdir()

    # The repo used as submodule (master branch)
    with open(submodule_dir / "damspam.yaml", "w") as fd:
        fd.write(
            "\n".join(
                (
                    "damspam:",
                    "  webhooks:",
                    "    - path: some/ns/other",
                    "      token: one-two-three",
                    "",
                )
            )
        )
    with open(submodule_dir / "bugbot.yaml", "w") as fd:
        fd.write(
            "\n".join(
                (
                    "bugbot:",
                    "  webhooks:",
                    "    - path: bbsome/ns/bbother",
                    "      token: four-five-six",
                    "",
                )
            )
        )

    subprocess.run(["git", "init", "-b", "master"], cwd=submodule_dir)
    subprocess.run(
        ["git", "config", "--local", "receive.denyCurrentBranch", "ignore"],
        cwd=submodule_dir,
    )
    subprocess.run(
        ["git", "config", "--local", "user.name", "freedesktop.org pytest"],
        cwd=submodule_dir,
    )
    subprocess.run(
        ["git", "config", "--local", "user.email", "pytest@fdo"],
        cwd=submodule_dir,
    )
    subprocess.run(["git", "add", "damspam.yaml"], cwd=submodule_dir)
    subprocess.run(["git", "add", "bugbot.yaml"], cwd=submodule_dir)
    subprocess.run(
        [
            "git",
            "commit",
            "--author='freedesktop.org pytest <pytest@fdo>'",
            "-m",
            "initial commit to test repo",
        ],
        cwd=submodule_dir,
    )

    # The parent module (main branch)
    subprocess.run(["git", "init", "-b", "main"], cwd=repo_dir)
    subprocess.run(
        ["git", "config", "--local", "receive.denyCurrentBranch", "ignore"],
        cwd=repo_dir,
    )
    subprocess.run(
        ["git", "config", "--local", "user.name", "freedesktop.org pytest"],
        cwd=repo_dir,
    )
    subprocess.run(
        ["git", "config", "--local", "user.email", "pytest@fdo"],
        cwd=repo_dir,
    )
    # protocol.file.allow=always is required to add from local directory
    subprocess.run(
        [
            "git",
            "-c",
            "protocol.file.allow=always",
            "submodule",
            "add",
            submodule_dir,
            submodule_dir.name,
        ],
        cwd=repo_dir,
    )
    subprocess.run(
        [
            "git",
            "commit",
            "--author='freedesktop.org pytest <pytest@fdo>'",
            "-m",
            "Add submodule in test repo",
        ],
        cwd=repo_dir,
    )

    return repo_dir


@pytest.mark.parametrize("bot", (BUGBOT, SPAMBOT))
@pytest.mark.parametrize("readonly", (True, False))
@patch("tempfile.TemporaryDirectory")
def test_secrets(tempdir, tmp_path, bot, readonly):
    # upstream is where we put our fake git repo that Secrets will clone from
    sourcedir = tmp_path / "upstream"
    sourcedir.mkdir()

    # Secrets creates its own TemporaryDirectory, patch that so all
    # our test data is in tmp_path for inspection
    fake_tempdir = tmp_path / "temporary_directory"
    fake_tempdir.mkdir()
    tempdir.return_value.name = fake_tempdir

    # this is the upstream repo
    repo_name = Path("fdo-bots")
    submodule_name = Path("helm-gitlab-secrets")  # hardcoded in Secrets
    repo_dir = init_fake_git_repo(sourcedir, repo_name, submodule_name)

    with Secrets.clone(
        bot=bot,
        repo_url=sourcedir / repo_name,
        readonly=readonly,
    ) as secret:
        assert secret.readonly == readonly

        secret.add_project_entry("foo/bar/baz", "top-secret")
        secret.push()

    # clone the updated repo to check if the changes got pushed back
    updated_repo = tmp_path / "check-repo.git"
    subprocess.run(["git", "clone", repo_dir, updated_repo], check=True)
    subprocess.run(
        [
            "git",
            "-c",
            "protocol.file.allow=always",
            "submodule",
            "update",
            "--init",
        ],
        cwd=updated_repo,
        check=True,
    )
    updated_data = open(updated_repo / submodule_name / f"{bot.project}.yaml").read()
    logger.debug(updated_data)
    yml = yaml.safe_load(updated_data)
    assert yml[bot.project]["webhooks"] is not None
    hook = yml[bot.project]["webhooks"][-1]
    logger.debug(hook)
    if readonly:
        if bot.project == "damspam":
            assert hook == {
                "path": "some/ns/other",
                "token": "one-two-three",
            }
        elif bot.project == "bugbot":
            assert hook == {
                "path": "bbsome/ns/bbother",
                "token": "four-five-six",
            }
    else:
        assert hook == {
            "path": "foo/bar/baz",
            "token": "top-secret",
        }


def test_bot():
    spambot = SPAMBOT
    assert spambot.username == "spambot"
    assert spambot.id == 85713
    bugbot = BUGBOT
    assert bugbot.username == "bugbot"
    assert bugbot.id == 94117


@pytest.mark.parametrize("protocol", ("http", "https"))
@pytest.mark.parametrize("host", ("gitlab.freedesktop.org", "example.com"))
@pytest.mark.parametrize("namespace", ("foo", "ns/subns"))
@pytest.mark.parametrize("project_name", ("bar", "bar.with.dots"))
@pytest.mark.parametrize("entity_type", (None, "issues", "snippets", "merge_requests"))
@pytest.mark.parametrize("entity_id", (123, 456789))
@pytest.mark.parametrize("note_id", (None, 12345))
def test_gitlab_url_parse(
    protocol, host, namespace, project_name, entity_type, entity_id, note_id
):
    url = f"{protocol}://{host}/{namespace}/{project_name}"

    entity_map = {
        "issues": "#",
        "snippets": "$",
        "merge_requests": "!",
    }
    instance = f"{protocol}://{host}"
    spec = f"{namespace}/{project_name}"  # {entity_map.get(entity_type, '')}{entity_id or ''}"

    for o in [
        GitlabUrl.from_url(url),
        GitlabUrl.from_url(url + "/"),
        GitlabUrl.from_spec(instance, spec),
    ]:
        assert o is not None
        assert o.instance == instance
        assert o.namespace == namespace
        assert o.project_name == project_name
        assert o.path_with_namespace == f"{namespace}/{project_name}"
        assert o.url_type == GitlabUrl.UrlType.PROJECT
        assert o.entity_type is None
        assert o.entity_id is None
        assert o.note_id is None
        assert o.url == url

    if entity_type is not None:
        incomplete_url = f"{url}/-/{entity_type}/"
        incomplete_spec = f"{namespace}/{project_name}{entity_map[entity_type]}"
        for o in [
            GitlabUrl.from_url(incomplete_url),
            GitlabUrl.from_spec(instance, incomplete_spec),
        ]:
            assert o is not None
            assert o.instance == instance
            assert o.namespace == namespace
            assert o.project_name == project_name
            assert o.path_with_namespace == f"{namespace}/{project_name}"
            assert o.url_type == GitlabUrl.UrlType.PROJECT
            assert o.entity_type is None
            assert o.entity_id is None
            assert o.note_id is None

        url = f"{url}/-/{entity_type}/{entity_id}"
        spec = f"{namespace}/{project_name}{entity_map[entity_type]}{entity_id}"
        for o in [GitlabUrl.from_url(url), GitlabUrl.from_spec(instance, spec)]:
            assert o is not None
            assert o.instance == instance
            assert o.namespace == namespace
            assert o.project_name == project_name
            assert o.path_with_namespace == f"{namespace}/{project_name}"
            if entity_type == "issues":
                assert o.url_type == GitlabUrl.UrlType.ISSUE
            elif entity_type == "snippets":
                assert o.url_type == GitlabUrl.UrlType.SNIPPET
            elif entity_type == "merge_requests":
                assert o.url_type == GitlabUrl.UrlType.MERGE_REQUEST
            assert o.entity_type == entity_type
            assert o.entity_id == entity_id
            assert o.note_id is None
            assert o.url == url

        if note_id is not None:
            url = f"{url}#note_{note_id}"
            spec = f"{spec}#note_{note_id}"
            for o in [GitlabUrl.from_url(url), GitlabUrl.from_spec(instance, spec)]:
                assert o is not None
                assert o.instance == instance
                assert o.namespace == namespace
                assert o.project_name == project_name
                assert o.path_with_namespace == f"{namespace}/{project_name}"
                if entity_type == "issues":
                    assert o.url_type == GitlabUrl.UrlType.ISSUE
                elif entity_type == "snippets":
                    assert o.url_type == GitlabUrl.UrlType.SNIPPET
                elif entity_type == "merge_requests":
                    assert o.url_type == GitlabUrl.UrlType.MERGE_REQUEST
                assert o.entity_type == entity_type
                assert o.entity_id == entity_id
                assert o.note_id == note_id
                assert o.url == url
