# damspam

A bot to dam the influx of GitLab spam. This bot is primarily invoked from
other bots but it can be invoked manually for other tasks.

## How it works

`damspam`, together with [hookiedookie](https://gitlab.freedesktop.org/freedesktop/hookiedookie/)
is already available on https://gitlab.freedesktop.org.


When project maintainer files a `damspam request-webhook` for their project,
`damspam` processes this request automatically and inserts web hooks into the
requesting project. These hooks are triggered whenever an issue is created or
updated. Note: the `hookiedookie` setup filters these events so `damspam` only
ever sees events for issues with the "Spam" label.

If an issue is assigned the "Spam" label, `damspam` blocks the issue creator
(the spammer), makes the issue confidential and recursively does the same for
every issue created by the spammer. This immediately removes spam from view.

Additionally, it allows anyone with permissions to assign a label to fight spam
- no more need to wait around for a GitLab administrator to go online and check
the abuse reports.

For GitLab administrators, `damspam` provides a convenient CLI to interactively
purge all users designated as spammers, see `damspam`

## Usage for GitLab project maintainers

This requires a user to have the Maintainer role or above. The user must
create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
with API access and save this token as `$XDG_CONFIG_HOME/damspam/user.token`.
```
$ pip install git+https://gitlab.freedesktop.org/freedesktop/damspam
$ damspam request-webhook knockknock/whoisthere
$ pip uninstall damspam
$ rm $XDG_CONFIG_HOME/damspam/user.token
```
See `damspam request-webhook --help` for further details on this command.

## Usage for GitLab administrators

This requires a user to have the Administrator role or above. The user must
create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
with API access and save this token as `$XDG_CONFIG_HOME/damspam/spambot.token`.
This token *should* be created for the **@spambot** user to impersonate the spambot.

```
$ pip install git+https://gitlab.freedesktop.org/freedesktop/damspam
$ damspam purge-spammers
```
The above command is safe to run. It does not modify data until confirmed by
the user. See `damspam purge-spammers --help` for more information.

To trigger `damspam` directly for a particular issue, use:
```
damspam hide-issue http://gitlab.freedesktop.org/knockknock/whoisthere/-/issues/123
```
Note that for this case you *definitely* want an API token to impersonate @spambot.

## The "Spam" label

Note that this bot hardcodes the "Spam" label in many places. If you want to
use the bot in your project, the label must match this spelling (i.e. not
"spam" or "SPAM").

Note that if the label does not exist it is automatically created as a
**Project Label**. In most cases it's better to create **Group Label** so
all projects in that group share the same label. This has to be done manually
until [this issue](https://gitlab.freedesktop.org/freedesktop/damspam/-/issues/3) is fixed.

## Usage for hacking on damspam

As always, `damspam --help` lists the commands available. Commands are grouped into
`foo-bar` and `hook-foo-bar`. The two commands are usually the same, but the `hook-`
variant is designed to work with the payload from a GitLab Webhook.

To trigger `damspam` for a fake GitLab Webhook for an issue event, use:

```
damspam hook-hide-issue --payload-from path/to/payload-file
damspam hook-hide-issue --payload-env PAYLOAD_VAR
```

To trigger `damspam` directly for a particular issue, use:
```
damspam hide-issue mynamespace/theproject 123
```

## The tracker issue

When hiding spam issues, the bot will link the hidden issue with the
freedesktop spam tracker issue:
[freedesktop/freedesktop#548](https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/548)
The intention here is that while the issues are hidden ("confidential"),
someone still needs to be able to see them to work through and purge them.

This is easiest by linking each spam issue into one central issue - unlinking
could eventually trigger another bot to undo the work of the spam label.

Linking to the tracker issue can be disabled with the `--no-tracker-issue`
argument. For testing, the tracker issue can be changed with the
`--tracker-issue` argument.

## Testing locally

Create an API token with API access and store its content in
`$XDG_CONFIG_HOME/damspam/spambot.token`. The token needs to have access to the
target projects and issues - for real changes this would be an Administrator
level token but for testing a personal token will work too.

To test the CLI for a specific issue, in this case run the following command.

You *really* should use `--readonly` to avoid making any actual changes. All
actions that the bot would but doesn't do are prefixed with `[RO SKIP]` in the
logs.

```
$ ./damspam.py \
   --readonly \
   --verbose \
   hide-issue
   https://gitlab.freedesktop.org/foo/bar/-/issues/2
```

The above runs, *in readonly* mode the `hide-issue` hook. To do the same but based on
a Webhook payload, save the issue event payload data (see below) in the file
`/tmp/issue-event-payload`:

```
# Note: this only works if the "Spam" label is set
$ ./damspam.py \
   --readonly \
   --verbose \
   hide-issue-hook
   --payload-file=/tmp/issue-event-payload.json
```

To get the payload data, go the the project Settings in GitLab, add a fake
Webhook for `http://example.com`, then click `Test`, `Issues Events` and
finally `View Details`. Note that for webhook tests, the `changes` JSON field is empty.
